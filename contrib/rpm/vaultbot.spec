%global provider        gitlab
%global provider_tld    com
%global project         msvechla
%global repo            vaultbot
%global provider_prefix %{provider}.%{provider_tld}/%{project}/%{repo}
%global import_path     %{provider_prefix}

%if ! 0%{?gobuild:1}
%define gobuild(o:) go build -ldflags "${LDFLAGS:-} -B 0x$(head -c20 /dev/urandom|od -An -tx1|tr -d ' \\n')" -a -v -x %{?**};
%endif

%if ! 0%{?gotest:1}
%define gotest() go test -ldflags "${LDFLAGS:-}" %{?**}
%endif

Name:           vaultbot
Version:        1.8.0
Release:        1
Summary:        Lightweight Hashicorp Vault PKI client, built for infrastructure automation
License:        MIT
URL:            https://gitlab.com/msvechla/vaultbot
Source0:        %{name}-%{version}.tar.gz
Source1:	%{name}@.service
Source2:	%{name}@.timer
Source3:	%{name}.sysconfig

# e.g. el6 has ppc64 arch without gcc-go, so EA tag is required
ExclusiveArch:  %{?go_arches:%{go_arches}}%{!?go_arches:%{ix86} x86_64 %{arm}}
# If go_compiler is not set to 1, there is no virtual provide. Use golang instead.
BuildRequires:  %{?go_compiler:compiler(go-compiler)}%{!?go_compiler:golang} >= 1.9
BuildRequires:  systemd
BuildRequires:  git
BuildRequires:  upx

%description
Lightweight Hashicorp Vault PKI client, built for infrastructure automation.
Automatically request and renew certificates generated inside vault via the 
PKI backend. By default Vaultbot will only renew certificates that are due 
for renewal within a specified period. Therefore Vaultbot is ideal for 
running at a fixed interval (e.g. crontab). This tool is also inspired by 
the well-known certbot for letsencrypt.

%prep
%autosetup -p1 -n go/src/%{provider_prefix}

%build
%gobuild -o bin/%{name} %{import_path} || exit 1

upx bin/%{name}

%install
install -D -m 0755 bin/%{name} %{buildroot}%{_bindir}/%{name}

install -D -m 0644 %{SOURCE1} %{buildroot}%{_unitdir}/%{name}@.service
install -D -m 0644 %{SOURCE2} %{buildroot}%{_unitdir}/%{name}@.timer
install -D -m 0644 %{SOURCE3} %{buildroot}%{_sysconfdir}/sysconfig/%{name}

%clean
rm -rf %{buildroot}
rm -rf %{_builddir}/*

%files
%{_bindir}/%{name}
%{_unitdir}/%{name}@.service
%{_unitdir}/%{name}@.timer
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}

%doc *.md 

%changelog
* Wed Jun 05 2019 fuero <fuerob@gmail.com> 1.8.0-1
- version bump

* Fri Feb 08 2019 fuero <fuerob@gmail.com> 1.6.0-1
- version bump

* Mon Jan 21 2019 fuero <fuerob@gmail.com> 1.5.0-1
- version bump

* Thu Sep 27 2018 fuero <fuerob@gmail.com> 1.2.2-1
- Adjusts timer units.
- Drops env var patch, was merged upstream.

* Fri Aug 10 2018 fuero <fuerob@gmail.com> 1.2.1-2
- Patch to fix incorrectly named env vars. Submitted to upstream.
- SystemD service & timer file

* Fri Aug 10 2018 fuero <fuerob@gmail.com> 1.2.1-1
- initial package

