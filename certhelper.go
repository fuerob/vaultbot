package main

import (
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"time"

	"github.com/hashicorp/vault/api"
	"github.com/hashicorp/vault/sdk/helper/certutil"
	log "github.com/sirupsen/logrus"
	"gitlab.com/msvechla/vaultbot/cli"

	//JKS extension dependencies
	"github.com/pavel-v-chernykh/keystore-go"
)

// requestCertificate returns the parsed certificate request response from vault
func requestCertificate(client *api.Client, options cli.Options) *certutil.ParsedCertBundle {

	log.Println("Requesting certificate...")

	rawCertData, err := client.Logical().Write(fmt.Sprintf("%s/issue/%s", options.PKI.Mount, options.PKI.RoleName), map[string]interface{}{
		"common_name":          options.PKI.CommonName,
		"alt_names":            options.PKI.AltNames,
		"ip_sans":              options.PKI.IPSans,
		"ttl":                  options.PKI.TTL,
		"exclude_cn_from_sans": options.PKI.ExcludeSans,
		"private_key_format":   options.PKI.PrivateKeyFormat,
	})

	if err != nil {
		log.Fatalf("Error issues certificate request: %s", err.Error())
	}

	log.Println("Certificate data received.")

	certData, parseErr := certutil.ParsePKIMap(rawCertData.Data)
	if parseErr != nil {
		log.Fatalf("Error parsing certificate: %s", parseErr.Error())
	}

	return certData
}

// writeCertificateData persists a certificate bundle to the filesystem
func writeCertificateData(parsedCertBundle *certutil.ParsedCertBundle, options cli.Options) {

	// pem bundle
	var pemBundleOut *os.File
	var err error

	if options.PKI.PEMBundlePath != "" {
		pemBundleOut, err = os.OpenFile(options.PKI.PEMBundlePath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
		if err != nil {
			log.Fatalf("Failed to open %s for writing: %s", options.PKI.PEMBundlePath, err)
		}
	}

	// private key
	keyOut, err := os.OpenFile(options.PKI.PrivKeyPath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("Failed to open %s for writing: %s", options.PKI.PrivKeyPath, err)
	}

	pem.Encode(keyOut, &pem.Block{Type: string(parsedCertBundle.PrivateKeyFormat), Bytes: parsedCertBundle.PrivateKeyBytes})
	if pemBundleOut != nil {
		pem.Encode(pemBundleOut, &pem.Block{Type: string(parsedCertBundle.PrivateKeyFormat), Bytes: parsedCertBundle.PrivateKeyBytes})
	}

	keyOut.Close()
	log.Printf("Wrote private key to: %s.", options.PKI.PrivKeyPath)

	// certificate
	certOut, err := os.Create(options.PKI.CertPath)
	if err != nil {
		log.Fatalf("Failed to open %s for writing: %s", options.PKI.CertPath, err)
	}

	pem.Encode(certOut, &pem.Block{Type: "CERTIFICATE", Bytes: parsedCertBundle.CertificateBytes})
	if pemBundleOut != nil {
		pem.Encode(pemBundleOut, &pem.Block{Type: "CERTIFICATE", Bytes: parsedCertBundle.CertificateBytes})
	}
	certOut.Close()
	log.Printf("Wrote certificate to: %s.", options.PKI.CertPath)

	// certificate chain
	chainOut, err := os.Create(options.PKI.CAChainPath)
	if err != nil {
		log.Fatalf("Failed to open %s for writing: %s", options.PKI.CAChainPath, err)
	}

	for _, cert := range parsedCertBundle.CAChain {
		pem.Encode(chainOut, &pem.Block{Type: "CERTIFICATE", Bytes: cert.Bytes})
		if pemBundleOut != nil {
			pem.Encode(pemBundleOut, &pem.Block{Type: "CERTIFICATE", Bytes: cert.Bytes})
		}
	}

	chainOut.Close()
	log.Printf("Wrote CA chain to: %s.", options.PKI.CAChainPath)

	if pemBundleOut != nil {
		pemBundleOut.Close()
		log.Printf("Wrote PEM Bundle to: %s.", options.PKI.PEMBundlePath)
	}
}

// flusing password from var as described in https://github.com/pavel-v-chernykh/keystore-go/blob/master/examples/pem/main.go
func zeroing(s []byte) {
	for i := 0; i < len(s); i++ {
		s[i] = 0
	}
	log.Printf("Zeroed JKS passphrase")
}

//Read a JAVA KeyStore
func readKeyStore(filename string, password []byte) keystore.KeyStore {
	log.Printf("Start reading JKS: %s", filename)
	f, err := os.Open(filename)
	defer f.Close()
	if err != nil {
		if os.IsNotExist(err) {
			log.Printf("No initial JKS at: %s : %s", options.PKI.JKSPath, err)
			return nil
		} else {
			log.Fatalf("Failed to open JKS at: %s with provided password for reading: %s", options.PKI.JKSPath, err)
		}
	}
	keyStore, err := keystore.Decode(f, password)
	if err != nil {
		if len(keyStore) != 0 {
			log.Fatalf("Failed to decode JKS file %s: %s  - structure %s", filename, err, keyStore)
		} else {
			log.Printf("JKS is empty: %s with %s", filename, err)
			keyStore = make(map[string]interface{})
		}
	}
	log.Printf("Read and decoded JKS: %s", filename)
	return keyStore
}

//Write a JAVA KeyStore
func writeKeyStore(keyStore keystore.KeyStore, filename string, password []byte) {
	log.Printf("Start writing JKS: %s", filename)
	o, err := os.Create(filename)
	defer o.Close()
	if err != nil {
		log.Fatalf("Failed to create JKS file %s: %s", filename, err)
	}
	err = keystore.Encode(o, keyStore, password)
	if err != nil {
		log.Fatalf("Failed to encode JKS File %s: %s - structure %s", filename, err, keyStore)
	}
	log.Printf("Wrote and coded JKS: %s", filename)
}

// writeJKSCertificateData persists a certificate bundle to the JAVA KeyStore
func writeJKSCertificateData(parsedCertBundle *certutil.ParsedCertBundle, options cli.Options) {
	var err error
	var chain keystore.Certificate
	var bundle []keystore.Certificate

	password := []byte(options.PKI.JKSPassword)
	defer zeroing(password)
	ks := readKeyStore(options.PKI.JKSPath, password)

	if err != nil {
		if os.IsNotExist(err) {
			log.Fatalf("No initial JKS at: %s : %s", options.PKI.JKSPath, err)
			ks = keystore.KeyStore{}
		} else {
			log.Fatalf("Failed to open JKS at: %s with provided password for reading: %s", options.PKI.JKSPath, err)
		}
	}

	if ks == nil {
		ks = keystore.KeyStore{}
	}

	cert := keystore.Certificate{
		Type:    "X509",
		Content: []byte(parsedCertBundle.CertificateBytes),
	}

	entryc1 := &keystore.TrustedCertificateEntry{
		Entry: keystore.Entry{
			CreationDate: time.Now(),
		},
		Certificate: cert,
	}

	ks[strings.ToLower(options.PKI.JKSCertAlias)] = entryc1
	bundle = append(bundle, cert)

	log.Printf("Added Cert to the JKS structure: %s with alias %s", cert, strings.ToLower(options.PKI.JKSCertAlias))

	for index, cert := range parsedCertBundle.CAChain {
		chain = keystore.Certificate{
			Type:    "X509",
			Content: []byte(cert.Bytes),
		}

		entryc2 := &keystore.TrustedCertificateEntry{
			Entry: keystore.Entry{
				CreationDate: time.Now(),
			},
			Certificate: chain,
		}

		ks[strings.ToLower(fmt.Sprintf("%s%s%d", options.PKI.JKSCAChainAlias, "_", index))] = entryc2
		bundle = append(bundle, chain)
		log.Printf("Added Cert to chain in the JKS structure: %s with alias %s", chain, strings.ToLower(fmt.Sprintf("%s%s%d", options.PKI.JKSCAChainAlias, "_", index)))
	}

	log.Printf("Added Chain to the JKS structure")

	entryp := &keystore.PrivateKeyEntry{
		Entry: keystore.Entry{
			CreationDate: time.Now(),
		},
		PrivKey:   []byte(parsedCertBundle.PrivateKeyBytes),
		CertChain: bundle,
	}

	ks[strings.ToLower(options.PKI.JKSPrivKeyAlias)] = entryp

	log.Printf("Added Private Key to the JKS structure")

	password = []byte(options.PKI.JKSPassword)
	defer zeroing(password)
	writeKeyStore(ks, options.PKI.JKSPath, password)

}

// readCurrentCertificate parses an existing certificate at the specified location
func readCurrentCertificate(options cli.Options) *x509.Certificate {
	if _, err := os.Stat(options.PKI.CertPath); err == nil {
		certFile, fileErr := ioutil.ReadFile(options.PKI.CertPath)
		if fileErr != nil {
			log.Fatalf("Unable to read certificate at %s: %s", options.PKI.CertPath, fileErr)
		}

		block, _ := pem.Decode([]byte(certFile))
		if block == nil {
			log.Fatalln("Failed to decode certificate PEM")
		}

		cert, err := x509.ParseCertificate(block.Bytes)
		if err != nil {
			log.Fatalf("Failed to parse certificate at %s: %s", options.PKI.CertPath, err.Error())
		}
		return cert
	}
	return nil
}

// readJKSCurrentCertificate parses an existing certificate at the specified JKS with the specified label
func readJKSCurrentCertificate(options cli.Options) *x509.Certificate {
	var err error

	password := []byte(options.PKI.JKSPassword)
	defer zeroing(password)
	ks := readKeyStore(options.PKI.JKSPath, password)

	if ks == nil {
		log.Printf("No initial JKS or JKS empty at: %s : %s", options.PKI.JKSPath, err)
		return nil
	} else {

		if err != nil {
			log.Fatalf("Failed to open JKS at: %s with provided password for reading: %s", options.PKI.JKSPath, err)
		}

		entry := ks[strings.ToLower(options.PKI.JKSCertAlias)]

		if entry == nil {
			log.Println("No cert in JKS")
			return nil
		} else {
			certEntry := entry.(*keystore.TrustedCertificateEntry)

			cert, err := x509.ParseCertificate([]byte(certEntry.Certificate.Content))

			if err != nil {
				log.Fatalf("Failed to parse certificate at JKS: %s for alias: %s : %s", options.PKI.JKSPath, options.PKI.JKSCertAlias, err.Error())
			}

			return cert
		}
	}
}

// isCertificateRenewalDue checks a certificates expiry based on selected options
func isCertificateRenewalDue(cert *x509.Certificate, options cli.Options) bool {

	if options.PKI.ForceRenew {
		log.Println("Force renewal is activated. Skipping renewal due check...")
		return true
	}

	log.Println("Checking certificate expiry...")

	if cert != nil {
		// check if certificate is due for renewal
		if options.PKI.RenewTime != "" {
			renewDuration, timeErr := time.ParseDuration(options.PKI.RenewTime)
			if timeErr != nil {
				log.Fatalf("Unable to parse renew_duration %s: %s", options.PKI.RenewTime, timeErr.Error())
			}

			if time.Now().Add(renewDuration).After(cert.NotAfter) {
				log.Printf("Certificate due for renewal, expires %s", cert.NotAfter)
				return true
			}
			log.Printf("Certificate not yet due for renewal, will be renewed %s before expiry (%s)", renewDuration, cert.NotAfter)
		} else {
			// calculate percentage
			if (options.PKI.RenewPercent < 0.0) || (options.PKI.RenewPercent > 1.0) {
				log.Fatalf("Error: renew_percent must be a value between 0.0 and 1.0, got: %f", options.PKI.RenewPercent)
			}

			ttl := cert.NotAfter.Sub(cert.NotBefore)
			percSecs := ttl.Seconds() * options.PKI.RenewPercent
			duration, timeErr := time.ParseDuration(fmt.Sprintf("%fs", percSecs))
			if timeErr != nil {
				log.Fatalf("Unable to parse / calculate renew_percent %s: %s", options.PKI.RenewTime, timeErr.Error())
			}

			if time.Now().After(cert.NotBefore.Add(duration)) {
				log.Printf("Certificate due for renewal, expires %s", cert.NotAfter)
				return true
			}
			log.Printf("Certificate not yet due for renewal, will be renewed after: %s (%s after creation)", cert.NotBefore.Add(duration), duration)
		}

		return false
	}

	log.Printf("No certificate found at: %s. Skipping renewal due check...", options.PKI.CertPath)
	return true
}

// hasCertificateDataChanged verifies whether the requested data matches the data from an already existing certificate on the specified location.
// This ensures that no certificate is overwritten by mistake
func hasCertificateDataChanged(cert *x509.Certificate, options cli.Options) bool {
	// check common name
	if strings.ToLower(cert.Subject.CommonName) != strings.ToLower(options.PKI.CommonName) {
		log.Printf("Common name changed: old(%s) vs new(%s)", cert.Subject.CommonName, options.PKI.CommonName)
		return true
	}

	// check dns sans
	dnsNames := strings.Split(options.PKI.AltNames, ",")
	if dnsNames[0] != "" {
		dnsNames = append(dnsNames, strings.ToLower(cert.Subject.CommonName))
	} else {
		dnsNames[0] = strings.ToLower(cert.Subject.CommonName)
	}

	if len(cert.DNSNames) != len(dnsNames) {
		log.Printf("Dns alt names changed: old(%s) vs new(%s)", cert.DNSNames, dnsNames)
		return true
	}

	for _, sn := range cert.DNSNames {
		if !contains(dnsNames, strings.ToLower(sn)) {
			log.Printf("Dns alt names changed: old(%s) vs new(%s)", cert.DNSNames, dnsNames)
			return true
		}
	}

	// check ip sans
	ipSans := strings.Split(options.PKI.IPSans, ",")
	if (ipSans[0] == "") && (len(ipSans) == 1) {
		ipSans = []string{}
	}

	if len(ipSans) != len(cert.IPAddresses) {
		log.Printf("IP SANs changed: old(%s) vs new(%s)", cert.IPAddresses, ipSans)
		return true
	}

	for _, ip := range cert.IPAddresses {
		if !contains(ipSans, ip.String()) {
			log.Printf("IP SANs changed: old(%s) vs new(%s)", cert.IPAddresses, ipSans)
			return true
		}
	}
	return false
}

// contains checks if array contains string
func contains(array []string, s string) bool {
	for _, i := range array {
		if i == s {
			return true
		}
	}
	return false
}
